using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

public class ProductContext(DbContextOptions<ProductContext> options) : DbContext(options)
{
    public DbSet<Bags> Bags { get; set; }
    public DbSet<Dresses> Dresses { get; set; }
}

[Table("bags")]
public class Bags
{
    [Column("id")]
    public int Id { get; set; }
    [Column("price")]
    public float Price { get; set; }
    [Column("model")]
    public string Model { get; set; }
    [Column("color")]
    public string Color { get; set; }
    [Column("with_handle")]
    public bool WithHandle { get; set; }
    [Column("shoulder_strap")]
    public bool ShoulderStrap { get; set; }
}

[Table("dresses")]
public class Dresses
{
    [Column("id")]
    public int Id { get; set; }
    [Column("price")]
    public decimal Price { get; set; }
    [Column("model")]
    public string Model { get; set; }
    [Column("color")]
    public string Color { get; set; }
    [Column("pockets")]
    public short Pockets { get; set; }
}
