class Player
{
    // We have to put these fields private
    public int Experience = 0;
    public int Level = 0;

    public void Kill(string monster)
    {
        int xp = 0;
        switch (monster)
        {
            case "orc":
                xp = 5;
                break;
            case "troll":
                xp = 20;
                break;
            case "ogre":
                xp = 50;
                break;
        }
        this.Experience += xp;
        this.LevelUp();
    }

    // We have to put this method private
    public void LevelUp()
    {
        if (this.Experience >= 1000)
        {
            this.Experience -= 1000;
            this.Level++;
        }
    }
}

