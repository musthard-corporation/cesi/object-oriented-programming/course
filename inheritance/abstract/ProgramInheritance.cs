class ProgramAbstract
{
    public static void Run()
    {
        // CS0144 - Cannot create an instance of the abstract type or interface 'Product'
        // Product other = new();

        ProductAbstract dress = new DressAbstract(25f, "model", "color", "size", 0);
        ProductAbstract bag = new BagAbstract(35f, "model", "color", true, false);
        DressAbstract? transtyped = bag.As<DressAbstract>();
        Console.WriteLine(transtyped == null);

        ProductAbstract[] products = [dress, bag];
        foreach (var product in products)
        {
            // Can use functions defined in Product abstract class
        }
    }
}