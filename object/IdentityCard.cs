class IdentityCard(
    string documentNumber,
    string surname,
    string[] givenNames,
    char sex,
    string nationality,
    DateTime birthDate,
    string birthPlace,
    string address,
    float height,
    DateTime? expiryDate = null
)
{
    private static readonly int AVAILABILITY_DURATION = 10;
    public readonly string DocumentNumber = documentNumber;
    public readonly string Surname = surname;
    public readonly string[] GivenNames = givenNames;
    public readonly char Sex = sex;
    public readonly string Nationality = nationality;
    public readonly DateTime BirthDate = birthDate;
    public readonly string BirthPlace = birthPlace;
    public readonly string Address = address;
    public readonly float Height = height;
    public readonly DateTime ExpiryDate = expiryDate ?? DateTime.Today.AddYears(AVAILABILITY_DURATION);

    //     public IdentityCard(
    //         string documentNumber,
    //         string surname,
    //         string[] givenNames,
    //         char sex,
    //         string nationality,
    //         DateTime birthDate,
    //         string birthPlace,
    //         string address,
    //         float height,
    //         DateTime? expiryDate = null
    //         )
    //     {
    //         DocumentNumber = documentNumber;
    //         Surname = surname;
    //         GivenNames = givenNames;
    //         Sex = sex;
    //         Nationality = nationality;
    //         BirthDate = birthDate;
    //         BirthPlace = birthPlace;
    //         Address = address;
    //         Height = height;
    //         ExpiryDate = expiryDate ?? DateTime.Today.AddYears(15);
    //     }

    // public IdentityCard() {
    //     DocumentNumber = "documentNumber";
    //     Surname = "surname";
    //     GivenNames = ["givenNames"];
    //     Sex = 'h';
    //     Nationality = "nationality";
    //     BirthDate = DateTime.Now;
    //     BirthPlace = "birthPlace";
    //     Address = "address";
    //     Height = 1.8F;
    //     ExpiryDate = DateTime.Today.AddYears(15);
    // }

    public string GetGivenNames()
    {
        return string.Join(", ", GivenNames);
    }

    public bool IsMan()
    {
        // TODO - Char depends on where the card is mad (H in France, M in England, ...)
        // Best practice : Create an enum to not depend on country
        return Sex == 'H';
    }

    public bool IsWoman()
    {
        // TODO - Char depends on where the card is mad (F in France, W in England, ...)
        // Best practice : Create an enum to not depend on country
        return Sex == 'F';
    }

    public int GetAge()
    {
        // TODO - Compute depending on current day (not good value if we are in june and birth is in august)
        return DateTime.Today.Year - BirthDate.Year;
    }

    public DateTime GetDeliveryDate()
    {
        return ExpiryDate.AddYears(-AVAILABILITY_DURATION);
    }

    public bool IsExpired()
    {
        return ExpiryDate < DateTime.Now;
    }
}

