using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql;
using System.Data.Odbc;

class ProgramDatabase
{
    const string HOST = "localhost";
    const string DATABASE = "products";
    const string USERNAME = "postgres";
    const string PASSWORD = "postgres";

    const string NPGSQL_CONNECTION_STRING = $"Host={HOST};Database={DATABASE};Username={USERNAME};Password={PASSWORD}";
    const string ODBC_CONNECTION_STRING = $"DRIVER={{PostgreSQL UNICODE(x64)}};SERVER={HOST};DATABASE={DATABASE};UID={USERNAME};PASSWORD={PASSWORD}";

    public static void Run()
    {
        // BasicConnection();
        // AdvancedConnection();
        // OdbcBasicExample();
        // OdbcAdvancedExample();
        // OrmExample();
    }

    private static void BasicConnection()
    {
        NpgsqlConnection connection = new(NPGSQL_CONNECTION_STRING);
        connection.Open();

        using var cmd = new NpgsqlCommand("SELECT model FROM bags", connection);
        using var reader = cmd.ExecuteReader();
        while (reader.Read())
            Console.WriteLine(reader.GetString(0));
    }

    private static void AdvancedConnection()
    {
        using var dataSource = NpgsqlDataSource.Create(NPGSQL_CONNECTION_STRING);

        DressModel example = new(null, 5, "npgsql-example", "npgsql-example", 0);
        var id = example.Create(dataSource);

        if (id == null)
        {
            Console.WriteLine("NPGSQL Example not inserted");
        }
        else
        {
            var exampleFromDb = DressModel.Get(dataSource, id.Value);
            Console.WriteLine(exampleFromDb);
        }
    }

    private static void OdbcBasicExample()
    {
        OdbcConnection connection = new(ODBC_CONNECTION_STRING);

        connection.Open();

        using var cmd = new OdbcCommand("SELECT model FROM bags", connection);
        using var reader = cmd.ExecuteReader();
        while (reader.Read())
            Console.WriteLine(reader.GetString(0));
    }

    private static void OdbcAdvancedExample()
    {
        OdbcConnection connection = new(ODBC_CONNECTION_STRING);

        connection.Open();

        DressModel example = new(null, 5, "odbc-example", "odbc-example", 0);
        var id = example.Create(connection);

        if (id == null)
        {
            Console.WriteLine("ODBC Example not inserted");
        }
        else
        {
            var exampleFromDb = DressModel.Get(connection, id.Value);
            Console.WriteLine(exampleFromDb);
        }
    }

    private static void OrmExample()
    {
        using var dbContext = new PooledDbContextFactory<ProductContext>(
                new DbContextOptionsBuilder<ProductContext>().UseNpgsql(NPGSQL_CONNECTION_STRING).Options
            )
            .CreateDbContext();

        var newDress = new Dresses { Price = 5, Model = "orm-example", Color = "orm-example", Pockets = 0 };
        dbContext.Dresses.Add(newDress);
        dbContext.SaveChanges();
        // Object is updated with values from database when saving

        Console.WriteLine($"There are {dbContext.Dresses.Count()} dresses");

        var example = dbContext.Dresses.Find(newDress.Id);
        if (example == null)
        {
            Console.WriteLine($"No dress found for id {newDress.Id}");
        }
        else
        {
            Console.WriteLine($"Id : {example.Id}\nPrice: {example.Price}\nModel: {example.Model}\nColor: {example.Color}\nPockets: {example.Pockets}");
        }

        // We could also query with SQL like command
        var dresses = from d in dbContext.Dresses where d.Id < 10 select d;
        Console.WriteLine($"There are {dresses.Count()} dresses with id under 10");
    }
}
