class ProgramMutlithreadingParallelism
{
    public static async Task Run()
    {
        ThreadsExample();

        await AsyncExample();
    }

    private static void ThreadsExample()
    {
        var threads = new List<Thread>
        {
            new(new ThreadStart(new ThreadExample(3).Wait)),
            new(new ThreadStart(new ThreadExample(1).Wait))
        };

        threads.ForEach(t => t.Start());

        Console.WriteLine("Waiting for threads");

        threads.ForEach(t => t.Join());

        Console.WriteLine("All threads finished !");
    }

    private static async Task AsyncExample()
    {
        await Task.Run(() => {
            // Do something
        });

        Console.WriteLine($" > {DateTime.Now} : Before single task");
        await Task.Delay(1000);
        Console.WriteLine($" > {DateTime.Now} : After single task");

        Task.WaitAll([Task.Delay(1000), Task.Delay(3000)]);
    }
}

class ThreadExample(int seconds)
{
    private readonly int WaitSeconds = seconds;

    public void Wait()
    {
        Console.WriteLine($" > {DateTime.Now} : Wait for {WaitSeconds} seconds");
        Thread.Sleep(WaitSeconds * 1000);
        Console.WriteLine($" > {DateTime.Now} : Finish waiting for {WaitSeconds} seconds");
    }
}