class DressInterface(float price, string model, string color, string size, int pockets) : IProduct
{
    readonly float Price = price;
    readonly string Model = model;
    readonly string Color = color;
    public readonly string Size = size;
    public readonly int Pockets = pockets;

    public float ComputeDiscount(int percentage)
    {
        return Price * (1 - percentage / 100);
    }

    public string[] GetAvailableColors()
    {
        return [Color, "other"];
    }

    public string[] GetAvailableSizes()
    {
        return [Size, "other"];
    }
}