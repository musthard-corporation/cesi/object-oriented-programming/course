using System.Text.Json.Serialization;

class City
{
    [JsonPropertyName("nom")]
    public required string Name { get; set; }
    [JsonPropertyName("code")]
    public required string Code { get; set; }
    [JsonPropertyName("codeDepartement")]
    public required string DepartmentCode { get; set; }
    [JsonPropertyName("siren")]
    public required string Siren { get; set; }
    [JsonPropertyName("codeEpci")]
    public required string EpciCode { get; set; }
    [JsonPropertyName("codeRegion")]
    public required string RegionCode { get; set; }
    [JsonPropertyName("codesPostaux")]
    public required string[] PostalCodes { get; set; }
    [JsonPropertyName("population")]
    public required int Population { get; set; }
    [JsonPropertyName("_score")]
    public float Score { get; set; }

    public override string ToString()
    {
        return $"{Name} ({Score})";
    }
}