class Dress(float price, string model, string size, string color, int pockets)
{
    readonly float Price = price;
    readonly string Model = model;
    readonly string Size = size;
    readonly string Color = color;
    readonly int Pockets = pockets;

    public float ComputeDiscount(int percentage)
    {
        return Price * (1 - percentage / 100);
    }

    public string[] GetAvailableSizes()
    {
        return [Size, "other"];
    }

    public string[] GetAvailableColors()
    {
        return [Color, "other"];
    }
}