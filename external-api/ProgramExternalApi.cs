using System.Text.Json;

class ProgramExternalApi
{
    const string CITY = "Dijon";

    public static async Task Run()
    {
        // https://geo.api.gouv.fr/decoupage-administratif/communes
    
        HttpClient client = new()
        {
            BaseAddress = new Uri("https://geo.api.gouv.fr/")
        };

        var response = await client.GetAsync($"communes?nom={CITY}");
        
        if (response.IsSuccessStatusCode)
        {
            var cities = JsonSerializer.Deserialize<City[]>(await response.Content.ReadAsStringAsync()) ?? [];
            Console.WriteLine(cities.Length);
            foreach (var city in cities) {
                Console.WriteLine(city);
            }
        }
    }
}
