class ProgramInheritance
{
    public static void Run()
    {
        ProductInheritance other = new(12f, "model", "color");
        ProductInheritance dress = new DressInheritance(25f, "model", "color", "size", 0);
        ProductInheritance bag = new BagInheritance(35f, "model", "color", true, false);

        ProductInheritance[] products = [other, dress, bag];
        foreach (var product in products)
        {
            // Can use fields/functions defined in Product class
            Console.WriteLine(product.Price);
            product.GetAvailableColors();
            // var castDress = product as DressInheritance;

            // To access specific subclass fields/functions, we have check and cast
            // var dressCasted = (DressInheritance) product;
            // var dressCasted = product as DressInheritance;
            if (product is DressInheritance dressCasted)
            {
                Console.WriteLine(dressCasted.Pockets);
            }
        }
    }
}