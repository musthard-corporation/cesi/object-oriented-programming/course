using System.Text;

class ProgramFile
{
    const string inexistantFilePath = @".\path\to\file.txt";
    const string existantFilePath = @".\README.md";
    const string newFilePath = @"./example/example.md";

    public static void Run()
    {
        // PathExample();
        // StaticExample();
        // InstanceExample();
    }

    private static void PathExample()
    {
        Console.WriteLine($"Directory of '{inexistantFilePath}' is '{Path.GetDirectoryName(inexistantFilePath)}'");
        Console.WriteLine($"Extension of '{existantFilePath}' is '{Path.GetExtension(existantFilePath)}'");
        Console.WriteLine($"Full path of '{existantFilePath}' is '{Path.GetFullPath(existantFilePath)}'");
    }

    private static void StaticExample()
    {
        Console.WriteLine("File '{0}' {1}", inexistantFilePath, File.Exists(inexistantFilePath) ? "exists" : "does not exists");

        Console.WriteLine("File '{0}' {1}", existantFilePath, File.Exists(existantFilePath) ? "exists" : "does not exists");

        Console.WriteLine();

        Console.WriteLine("Content of file '{0}' :\n{1}", existantFilePath, string.Join("\n", File.ReadAllLines(existantFilePath)));

        Console.WriteLine();

        var newFileParentDir = Path.GetDirectoryName(newFilePath);

        if (!Path.Exists(newFilePath))
        {
            if (newFileParentDir != null && !Directory.Exists(newFileParentDir))
            {
                Console.WriteLine($"Directory '{newFileParentDir}' does not exist -> Creating it");
                Directory.CreateDirectory(newFileParentDir);
            }

            File.Copy(existantFilePath, newFilePath);
        }

        File.WriteAllText(newFilePath, "File created programmatically with dotnet !");

        Console.WriteLine("Content of file '{0}' :\n{1}", newFilePath, string.Join("\n", File.ReadAllLines(newFilePath)));

        if (newFileParentDir == null)
        {
            File.Delete(newFilePath);
        }
        else
        {
            Directory.Delete(newFileParentDir, true);
        }
    }

    private static void InstanceExample()
    {
        var inexistantFile = new FileInfo(inexistantFilePath);
        Console.WriteLine("File '{0}' {1}", inexistantFile, inexistantFile.Exists ? "exists" : "does not exists");

        var existantFile = new FileInfo(existantFilePath);
        Console.WriteLine("File '{0}' {1}", existantFile, existantFile.Exists ? "exists" : "does not exists");

        Console.WriteLine();

        // StreamReader? fileStreamExample = null;
        // try
        // {
        //     fileStreamExample = existantFile.OpenText();
        //     Console.WriteLine("Content of file '{0}' :\n{1}", existantFile, fileStreamExample.ReadToEnd());
        // }
        // catch {
        //     // TODO
        // }
        // finally {
        //     fileStreamExample?.Close();
        // }

        using (var fileStream = existantFile.OpenText())
        {
            Console.WriteLine("Content of file '{0}' :\n{1}", existantFile, fileStream.ReadToEnd());
        }

        Console.WriteLine();

        FileInfo newFile = new(newFilePath);
        if (!newFile.Exists)
        {
            if (newFile.Directory != null && !newFile.Directory.Exists)
            {
                newFile.Directory.Create();
            }
            newFile = existantFile.CopyTo(newFilePath);
        }

        using (var fileStream = newFile.Open(FileMode.Truncate))
        {
            fileStream.Write(new UTF8Encoding().GetBytes("File created programmatically with dotnet !"));
            fileStream.Flush();
        }

        using (var fileStream = newFile.OpenText())
        {
            Console.WriteLine("Content of file '{0}' :\n{1}", newFile, fileStream.ReadToEnd());
        }

        if (newFile.Directory == null)
        {
            newFile.Delete();
        }
        else
        {
            newFile.Directory.Delete(true);
        }
    }
}