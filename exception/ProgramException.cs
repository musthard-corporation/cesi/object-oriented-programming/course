class ProgramException
{
    public static void Run()
    {
        try
        {
            Example(false);
            Example(true);
        }
        catch
        {
            Console.WriteLine("In catch statement but don't need the error");
        }
    }

    /// <summary>
    /// Basic example
    /// </summary>
    /// <param name="throwError">Indicate if a trhow has to be done</param>
    /// <exception cref="Exception">Thrown when param <c>error</c> is true</exception>
    private static void Example(bool throwError)
    {
        try
        {
            if (throwError)
            {
                throw new Exception("Error !!!");
            }
            else
            {
                Console.WriteLine("Everything is fine");
            }
        }
        catch (Exception e)
        {
            throw new Exception($"Just catched error : {e.Message}");
        }
        finally
        {
            Console.WriteLine("Statement always executed, whether there was a caught exception or not");
        }
    }

    /// <summary>
    /// Example of Try* method
    /// </summary>
    /// <returns>A int if parse is successful, or null if not</returns>
    private static int? TryExample()
    {
        // Don't
        var value = "test";
        try
        {
            Console.WriteLine($"Value '{int.Parse(value)}' was parsed from a string");
        }
        catch (ArgumentNullException)
        {
            Console.WriteLine("Argument null exception !");
        }
        catch (FormatException)
        {
            Console.WriteLine("Format exception !");
        }
        catch (OverflowException)
        {
            Console.WriteLine("Overflow exception !");
        }

        // Do
        if (int.TryParse(value, out int resultValue))
        {
            Console.WriteLine($"Value '{resultValue}' was parsed from a string");
            return resultValue;
        }
        else
        {
            Console.WriteLine($"Cannot parse '{value}' to int");
        }

        return null;
    }
}