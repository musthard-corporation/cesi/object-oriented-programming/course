class DressAbstract(float price, string model, string color, string size, int pockets) : ProductAbstract(price, model, color)
{
    public readonly string Size = size;
    public readonly int Pockets = pockets;

    public string[] GetAvailableSizes()
    {
        return [Size, "other"];
    }
}