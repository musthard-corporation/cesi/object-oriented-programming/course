﻿using System.Text;

class ProgramBasics
{
    public static void Run()
    {
        // See https://aka.ms/new-console-template for more information
        Console.WriteLine("Hello, World!");

        #region Types primitifs

        bool boolean = true;

        char caractere = 'A';
        string chaineDeCaractere = "ma chaine";

        short smallInt = 36;
        int integer = 36;
        long longInt = 36;

        float smallDecimal = 3.6F;
        double longDecimal = 3.6;

        #endregion

        #region Concatenation de string

        // un, deux 1
        string concatenate = "un" + ", " + "deux " + 1;
        // un, deux 12
        concatenate = concatenate + 2;
        // un, deux 123
        concatenate += 3;

        // un, deux 123 truc bidule 3.6
        string concatExample1 = concatenate + " truc bidule " + longDecimal;
        string concatExample2 = $"{concatenate} truc bidule {longDecimal}";
        
        var strBuilder = new StringBuilder();
        strBuilder.Append(concatenate);
        strBuilder.Append(" truc bidule ");
        strBuilder.Append(longDecimal);
        string concatExample3 = strBuilder.ToString();

        #endregion

        #region Tableau et listes

        int[] intArray = new int[1];
        intArray[0] = 0;
        // Index out of range
        // intArray[1] = 0;

        List<int> list = new List<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);
        list[0] = 1;
        list.Find((element) => element >= 2);

        Dictionary<int, string> dict = new Dictionary<int, string>();
        dict.Add(0, "zero");
        dict.Add(1, "un");

        #endregion

        #region Conditions

        if (list.Contains(0))
        {
            Console.WriteLine("zero exists");
        }
        else
        {
            // ...
        }

        switch (intArray[0])
        {
            case 0:
            case 1:
                // Todo
                break;
            case 2:
                // Todo
                break;
            default:
                // TODO
                break;
        }

        var t = list.Contains(0) ? "Zero exists" : "Zero not exists";

        #endregion

        #region Loop

        bool exitLoop = false;
        while (true)
        {
            if (exitLoop)
            {
                break;
            }
            exitLoop = true;
        }

        exitLoop = false;
        do
        {
            if (exitLoop)
            {
                break;
            }
            exitLoop = true;
        } while (false);

        for (int i = 0; i < 3; i++)
        {
            // TODO
        }

        foreach (var item in list)
        {
            // TODO
        }

        list.ForEach((element) => { Console.WriteLine(element); });

        #endregion

        #region Fonctions
        // <type de retour> <nom de la fonction>(<parametres>) {}

        void maFonction()
        {
            // TODO
            return;
        }

        #endregion
    }
}