class ProgramInterface
{
    public static void Run()
    {
        // CS0144 - Cannot create an instance of the abstract type or interface 'Product'
        // IProduct other = new();

        IProduct dress = new DressInterface(25f, "model", "color", "size", 0);
        IProduct bag = new BagInterface(35f, "model", "color", true, false);

        IProduct[] products = [dress, bag];
        foreach (var product in products)
        {
            // Can use functions defined in Product interface
        }
    }
}