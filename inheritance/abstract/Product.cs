abstract class ProductAbstract(float price, string model, string color)
{
    public readonly float Price = price;
    public readonly string Model = model;
    public readonly string Color = color;

    public float ComputeDiscount(int percentage)
    {
        return Price * (1 - percentage / 100);
    }

    public virtual string[] GetAvailableColors()
    {
        return [Color, "other"];
    }

    public T? As<T>() where T : class
    {
        return this as T;
    }
}