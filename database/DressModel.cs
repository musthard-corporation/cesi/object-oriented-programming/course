using System.Data.Odbc;
using Npgsql;

class DressModel(int? id, decimal price, string model, string color, short pockets)
{
    readonly int? Id = id;
    readonly decimal Price = price;
    readonly string Model = model;
    readonly string Color = color;
    readonly short Pockets = pockets;

    public int? Create(NpgsqlDataSource dataSource)
    {
        using (var cmd = dataSource.CreateCommand("INSERT INTO dresses (price, model, color, pockets) VALUES ($1, $2, $3, $4) RETURNING id"))
        {
            cmd.Parameters.AddWithValue(this.Price);
            cmd.Parameters.AddWithValue(this.Model);
            cmd.Parameters.AddWithValue(this.Color);
            cmd.Parameters.AddWithValue(this.Pockets);

            return (int?)cmd.ExecuteScalar();
        }
    }

    public int? Create(OdbcConnection connection)
    {
        using (var cmd = connection.CreateCommand())
        {
            cmd.CommandText = "INSERT INTO dresses (price, model, color, pockets) VALUES (?, ?, ?, ?) RETURNING id";
            cmd.Parameters.AddWithValue("price", this.Price);
            cmd.Parameters.AddWithValue("model", this.Model);
            cmd.Parameters.AddWithValue("color", this.Color);
            cmd.Parameters.AddWithValue("pockets", this.Pockets);

            return (int?)cmd.ExecuteScalar();
        }
    }

    public static DressModel? Get(NpgsqlDataSource dataSource, int id)
    {
        using (var cmd = dataSource.CreateCommand("SELECT * FROM dresses WHERE id = $1"))
        {
            cmd.Parameters.AddWithValue(id);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                return new(
                    reader.GetInt32(0),
                    reader.GetDecimal(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetInt16(4)
                );
            }
        }

        return null;
    }

    public static DressModel? Get(OdbcConnection connection, int id)
    {
        using (var cmd = connection.CreateCommand())
        {
            cmd.CommandText = "SELECT * FROM dresses WHERE id = ?";
            cmd.Parameters.AddWithValue("id", id);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                return new(
                    reader.GetInt32(0),
                    reader.GetDecimal(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetInt16(4)
                );
            }
        }

        return null;
    }

    public override string ToString()
    {
        return $"Id : {Id}\nPrice: {Price}\nModel: {Model}\nColor: {Color}\nPockets: {Pockets}";
    }
}