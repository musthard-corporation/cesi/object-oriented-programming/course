interface IProduct
{

    public float ComputeDiscount(int percentage);

    public string[] GetAvailableColors();
}