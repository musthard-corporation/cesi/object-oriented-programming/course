class BagAbstract(float price, string model, string color, bool withHandle, bool shoulderStrap) : ProductAbstract(price, model, color)
{
    readonly bool WithHandle = withHandle;
    readonly bool ShoulderStrap = shoulderStrap;

    public override string[] GetAvailableColors()
    {
        return [.. base.GetAvailableColors(), "more"];
    }
}