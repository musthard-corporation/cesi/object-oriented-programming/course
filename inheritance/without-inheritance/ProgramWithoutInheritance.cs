class ProgramWithoutInheritance
{
    public static void Run()
    {
        List<Dress> dresses = new();
        List<Bag> bags = new();

        // Impossible
        // List<Dress | Bag> products = new();
    }
}
