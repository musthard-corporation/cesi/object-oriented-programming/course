class ProgramConsole
{
    public static void Run()
    {
        Console.WriteLine("Append text and end with line break");
        Console.Write("Append");
        Console.Write(" text");
        Console.Write(" to");
        Console.Write(" current");
        Console.Write(" line");

        Console.WriteLine();

        Console.Write("\nEnter something and press enter : ");
        var lineInput = Console.ReadLine();
        Console.WriteLine($"You've just enter : '{lineInput}'");

        Console.WriteLine();

        Console.Write("Press any key :");
        var keyInput = Console.ReadKey();
        Console.WriteLine($"\nYou've just hit the key : '{keyInput.Key}'");

        Console.WriteLine();

        Console.Write("Enter something and press enter :");
        var input = Console.Read();
        Console.WriteLine($"\nThe first character of you input is : '{Convert.ToChar(input)}'");

        Console.BackgroundColor = ConsoleColor.DarkYellow;
        Console.ForegroundColor = ConsoleColor.DarkBlue;

        Console.WriteLine();
        Console.WriteLine("You can also change console colors !");
        Console.WriteLine();

        Console.ResetColor();

        Console.WriteLine();
    }
}