CREATE TABLE IF NOT EXISTS bags(
    id SERIAL PRIMARY KEY,
    price DECIMAL NOT NULL,
    model VARCHAR NOT NULL,
    color VARCHAR NOT NULL,
    with_handle BOOLEAN NOT NULL,
    shoulder_strap BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS dresses(
    id SERIAL PRIMARY KEY,
    price DECIMAL NOT NULL,
    model VARCHAR NOT NULL,
    color VARCHAR NOT NULL,
    pockets SMALLINT NOT NULL
);

INSERT INTO bags (price, model, color, with_handle, shoulder_strap) VALUES
(32.99, 'Leather', 'Brown', TRUE, TRUE),
(98.29, 'Textile', 'Black', FALSE, TRUE),
(98.29, 'Leather', 'Black', FALSE, FALSE),
(98.29, 'Wood', 'Brown', FALSE, FALSE),
(98.29, 'Unknown', 'Unknown', TRUE, FALSE);

INSERT INTO dresses (price, model, color, pockets) VALUES
(15.98, 'Lether', 'Black', 2),
(15.98, 'Textile', 'White', 1),
(15.98, 'Meat', 'Red/White', 1),
(15.98, 'Textile', 'Red', 0);
