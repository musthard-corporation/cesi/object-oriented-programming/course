class ProgramEncapsulation
{
    public static void Run()
    {
        Player example = new();
        // Problem : We don't want user can change directly this value
        example.Experience = 9999;
        // Problem : We don't want user to be able to call this method
        example.LevelUp();
    }
}
