class BagInheritance : ProductInheritance
{
    readonly bool WithHandle;
    readonly bool ShoulderStrap;

    public BagInheritance(float price, string model, string color) : base(price, model, color)
    {
        this.WithHandle = false;
        this.ShoulderStrap = false;
    }

    public BagInheritance(float price, string model, string color, bool withHandle, bool shoulderStrap) : base(price, model, color)
    {
        this.WithHandle = withHandle;
        this.ShoulderStrap = shoulderStrap;
    }

    public override string[] GetAvailableColors()
    {
        return [.. base.GetAvailableColors(), "more"];
    }
}

// class BagInheritance(float price, string model, string color, bool withHandle, bool shoulderStrap) : ProductInheritance(price, model, color)
// {
//     readonly bool WithHandle = withHandle;
//     readonly bool ShoulderStrap = shoulderStrap;

//     public override string[] GetAvailableColors()
//     {
//         return [.. base.GetAvailableColors(), "more"];
//     }
// }