class Bag(float price, string model, string color, bool withHandle, bool shoulderStrap)
{
    readonly float Price = price;
    readonly string Model = model;
    readonly string Color = color;
    readonly bool WithHandle = withHandle;
    readonly bool ShoulderStrap = shoulderStrap;

    public float ComputeDiscount(int percentage)
    {
        return Price * (1 - percentage / 100);
    }

    public string[] GetAvailableColors()
    {
        return [Color, "other"];
    }
}