class Trip(Flight OutwardFlight, Flight ReturnFlight, Hotel Hotel, CarRental? CarRental)
{
    readonly Flight OutwardFlight = OutwardFlight;
    readonly Flight ReturnFlight = ReturnFlight;
    readonly Hotel Hotel = Hotel;
    readonly CarRental? CarRental = CarRental;

    public void book()
    {
        Console.WriteLine($"Booking trip with outward flight {OutwardFlight}, return flight {ReturnFlight}, hotel {Hotel} and car rental {CarRental}");
    }
}

class Flight
{
    string Company;
    string FlyNumber;
    string Airport;
    string Gate;
    DateTime Date;
    double FlyDuration;
}

class Hotel
{
    string Country;
    string City;
    string name;
    DateOnly ArrivalDate;
    int NightsNumber;
}

class CarRental { }