﻿class ProgramObject
{
    public static void Run()
    {
        var date1 = new DateTime(2000, 1, 1);
        DateTime date2 = new(2000, 1, 1);

        IdentityCard maelysGaelle = new(
            "X4RTBPFW4",
            "MARTIN",
            ["Maëlys-Gaëlle", "Marie"],
            'F',
            "FRA",
            new(1990, 7, 13),
            "Paris",
            @"44 rue DÉSIRÉ SAINT CLEMENT
            RÉSIDENCE DU PLEIN AIR BAT 4
            33000 BORDEAUX
            FRANCE",
            1.68f,
            new(2030, 2, 11)
        );
    }
}
